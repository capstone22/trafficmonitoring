import React from 'react';
import {PartnerData} from  './PartnerData.js';
import Chart from './Chart.js';

// this fetches the data, later it will be used to graph a line graph with axises time vs #of transactions
// time will be hourly, thus will only use the first value in the timestamp, ex. 4:35 will only use 4
// hour will be used to plot x-axis location on the graph
export default class DataLoader extends React.Component {
    constructor(props) {
      super(props);
      this.state = {
        error: null,
        isLoaded: false,
        items: []
      };
    }
  
    componentDidMount() {
        // fake for now, not using it anyway, using "PartnerData" json file as placeholder
      fetch("http://dummy.restapiexample.com/api/v1/employees")
        .then(res => res.json())
        .then(
          (result) => {
            this.setState({
              isLoaded: true,
              //items: result.items
              items: PartnerData
            });
          },
          (error) => {
            this.setState({
              isLoaded: true,
              error
            });
          }
        )
    }
  
    render() {
      const { error, isLoaded, items } = this.state;
      if (error) {
        return <div>Error: {error.message}</div>;
      } else if (!isLoaded) {
        return <div>Loading...</div>;
      } else {
        return (<Chart rawData={items}/>);
       // return dataParser(items); 
      }
    }
  }