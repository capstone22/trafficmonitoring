import React, { Component } from 'react';
import LineChart from 'react-linechart';
import '../App.css';
import PropTypes from "prop-types";
//import '../node_modules/react-linechart/dist/styles.css';
 
const PERIOD = 23;
var XMIN = 23;
var XMAX = 0;
var YMAX = 10;
var YMIN = 9999;
export default class Chart extends Component {
    // the timeframe is defined by the data given, so if there is a range of 35 hours between the data
    // the graph x-axis will be 35 hours

    // Have a diff graph for each seperate day

    render() {
        return (
            <div>
                <div className="chart">
                    <div className="header2"> Traffic map for {getDate()}</div>
                    <LineChart
                        xLabel="Time (UTC)"
                        yLabel="Hits"
                        width={1100}
                        ticks={PERIOD}
                        height={500}
                        data={dataParser(this.props.rawData)}
                        yMin={YMIN}
                        yMax={YMAX}
                        xMin={XMIN}
                        xMax={XMAX}
                    />
                </div>				
            </div>
        );
    }
}
Chart.propTypes = {
    rawData: PropTypes.func
}

export const dataParser = (items) =>{
    var timeMap = new Map();
    var timePlots = [];
    if (items == null) return [{x: 1, y: 2}];

    items.map(item => {
        var key = item.timestamp.time;
        var str = key.split(':');
        key = str[0];
        if (timeMap.has(key)){
            var value = timeMap.get(key) + 1;
            timeMap.set(key, value);
        }
        else timeMap.set(key, 1);
    });

    var j = 1;
    while (j <= PERIOD){
        if (timeMap.has(`${j}`)) {
            if (j < XMIN) XMIN = j;
            if (j > XMAX) XMAX = j;
            const yValue = timeMap.get(`${j}`);
            if (yValue > YMAX) YMAX = yValue + 2;
            if (yValue < YMIN) {
                if (yValue > 2) YMIN = yValue - 2;
                else YMIN = 0;
            }
            timePlots.push({x: `${j}`, y: `${yValue}`})
        }
        j++;
    }

    // timePlots = [{x: 1, y: 2}, {x: 4, y: 3}, {x: 5, y: 5}];
    const data = [
        {									
            color: "#ccb500", 
            points: timePlots
        }
    ];
    return data;
  }

  const getDate = () =>{
    var today = new Date();
    var dd = String(today.getDate()).padStart(2, '0');
    var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
    var yyyy = today.getFullYear();

    today = mm + '/' + (dd-1) + '/' + yyyy;
    return today;
}