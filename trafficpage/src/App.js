import React from 'react';
import './App.css';
//import Chart from './components/Chart.js';
import DataLoader from './components/DataLoader.js';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <img src={"https://www.expediagroup.com/wp-content/uploads/2013/04/EPS-logo-full-color_480x322.png"} className="App-logo" alt="logo" />
        <p><DataLoader/></p>
      </header>
    </div>
  );
}

export default App;
